#!/bin/bash
# figure out where this script lives
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
echo "linking files to home directory..."
VIMRC=${DIR}/.vimrc
TMUXCONF=${DIR}/.tmux.conf
BASHRC=${DIR}/.bashrc
ln -sf $VIMRC ~/.vimrc
ln -sf $TMUXCONF ~/.tmux.conf

# yeah, I realize I should check if it's already been added first. Easy enough to do, but then I'd
# also have to check if it's up to date, which is slightly more involved. I'll do it later. Maybe.
# append our .bashrc to the host's .bashrc
cat ${DIR}/.bashrc >> ~/.bashrc



