### THIS WAS COPIED HERE BY DEVIN'S link.sh SCRIPT ###
# export variables
export VISUAL=vim
export EDITOR="$VISUAL" 

# useful functions/aliases
alias ll="ls -la"
repeat(){
    while true; do "$@"; done
}

bell() {
    echo "$@"
    "$@"; echo -e "\a"
}

ring() {
    echo -e "\a"
}

alias ll='ls -vlha'
alias grep='grep --color=always'
alias less='less -R'

# purple prompt
export PS1='\[$(tput setaf 2)\]Finished at \D{%T}\[$(tput setaf 5)\]\n[\u@\h \w] \[$(tput sgr0)\]\n$ '

clip () { cat "$@" | xclip -selection clipboard; }

### END COPIED SECTION ###



