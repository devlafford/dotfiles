" typing behavior
" set cursorline
set modeline
set autoindent
set expandtab
set shiftwidth=4
set softtabstop=4
set tabstop=8
set scrolloff=15
set esckeys
set ignorecase

" numbers and lines 
set number
set statusline=%F%m%r%h%w%<\ %{&ff}\ %Y\ [0x\%02.2B]\ %=l/%L,%v\ %p%%
set laststatus=2

" navigation with ijkl or mouse, insert with ; 
noremap ; i
noremap l l
noremap i k
noremap k j
noremap j h
nnoremap <C-k> <PageDown>
nnoremap <C-i> <PageUP>
nnoremap <C-l> <End>
nnoremap <C-j> <Home>
set mouse=a


" doing things while in insert mode
" navigation


" a e s t h e t i c s 
syntax on
color desert
set hlsearch

" syntastic?

set backspace=indent,eol,start
